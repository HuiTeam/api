/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ApiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MxpmsSearchEquipmentMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/mxpms-search-equipment/mxpms-search-equipment-my-suffix-detail.component';
import { MxpmsSearchEquipmentMySuffixService } from '../../../../../../main/webapp/app/entities/mxpms-search-equipment/mxpms-search-equipment-my-suffix.service';
import { MxpmsSearchEquipmentMySuffix } from '../../../../../../main/webapp/app/entities/mxpms-search-equipment/mxpms-search-equipment-my-suffix.model';

describe('Component Tests', () => {

    describe('MxpmsSearchEquipmentMySuffix Management Detail Component', () => {
        let comp: MxpmsSearchEquipmentMySuffixDetailComponent;
        let fixture: ComponentFixture<MxpmsSearchEquipmentMySuffixDetailComponent>;
        let service: MxpmsSearchEquipmentMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ApiTestModule],
                declarations: [MxpmsSearchEquipmentMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MxpmsSearchEquipmentMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(MxpmsSearchEquipmentMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MxpmsSearchEquipmentMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MxpmsSearchEquipmentMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MxpmsSearchEquipmentMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.mxpmsSearchEquipment).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
