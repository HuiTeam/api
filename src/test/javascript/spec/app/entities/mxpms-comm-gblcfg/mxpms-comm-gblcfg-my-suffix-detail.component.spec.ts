/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ApiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MxpmsCommGblcfgMySuffixDetailComponent } from '../../../../../../main/webapp/app/entities/mxpms-comm-gblcfg/mxpms-comm-gblcfg-my-suffix-detail.component';
import { MxpmsCommGblcfgMySuffixService } from '../../../../../../main/webapp/app/entities/mxpms-comm-gblcfg/mxpms-comm-gblcfg-my-suffix.service';
import { MxpmsCommGblcfgMySuffix } from '../../../../../../main/webapp/app/entities/mxpms-comm-gblcfg/mxpms-comm-gblcfg-my-suffix.model';

describe('Component Tests', () => {

    describe('MxpmsCommGblcfgMySuffix Management Detail Component', () => {
        let comp: MxpmsCommGblcfgMySuffixDetailComponent;
        let fixture: ComponentFixture<MxpmsCommGblcfgMySuffixDetailComponent>;
        let service: MxpmsCommGblcfgMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ApiTestModule],
                declarations: [MxpmsCommGblcfgMySuffixDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MxpmsCommGblcfgMySuffixService,
                    JhiEventManager
                ]
            }).overrideTemplate(MxpmsCommGblcfgMySuffixDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MxpmsCommGblcfgMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MxpmsCommGblcfgMySuffixService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MxpmsCommGblcfgMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.mxpmsCommGblcfg).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
