package com.sgcc.pms.api.web.rest;

import com.sgcc.pms.api.ApiApp;

import com.sgcc.pms.api.domain.MxpmsCommGblcfg;
import com.sgcc.pms.api.repository.MxpmsCommGblcfgRepository;
import com.sgcc.pms.api.service.dto.MxpmsCommGblcfgDTO;
import com.sgcc.pms.api.service.mapper.MxpmsCommGblcfgMapper;
import com.sgcc.pms.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MxpmsCommGblcfgResource REST controller.
 *
 * @see MxpmsCommGblcfgResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApp.class)
public class MxpmsCommGblcfgResourceIntTest {

    private static final String DEFAULT_GLBCFG_KEY = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_GLBCFG_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_GLBCFG_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_GLBCFG_ISSYSTEM = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_ISSYSTEM = "BBBBBBBBBB";

    private static final Instant DEFAULT_GLBCFG_CTIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_GLBCFG_CTIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_GLBCFG_DESC = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_DESC = "BBBBBBBBBB";

    private static final Long DEFAULT_GLBCFG_DISPIDX = 1L;
    private static final Long UPDATED_GLBCFG_DISPIDX = 2L;

    private static final String DEFAULT_GLBCFG_ID = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_ID = "BBBBBBBBBB";

    private static final String DEFAULT_GLBCFG_ISENCRYPT = "AAAAAAAAAA";
    private static final String UPDATED_GLBCFG_ISENCRYPT = "BBBBBBBBBB";

    @Autowired
    private MxpmsCommGblcfgRepository mxpmsCommGblcfgRepository;

    @Autowired
    private MxpmsCommGblcfgMapper mxpmsCommGblcfgMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMxpmsCommGblcfgMockMvc;

    private MxpmsCommGblcfg mxpmsCommGblcfg;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MxpmsCommGblcfgResource mxpmsCommGblcfgResource = new MxpmsCommGblcfgResource(mxpmsCommGblcfgRepository, mxpmsCommGblcfgMapper);
        this.restMxpmsCommGblcfgMockMvc = MockMvcBuilders.standaloneSetup(mxpmsCommGblcfgResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MxpmsCommGblcfg createEntity(EntityManager em) {
        MxpmsCommGblcfg mxpmsCommGblcfg = new MxpmsCommGblcfg()
            .glbcfgKey(DEFAULT_GLBCFG_KEY)
            .glbcfgValue(DEFAULT_GLBCFG_VALUE)
            .glbcfgName(DEFAULT_GLBCFG_NAME)
            .glbcfgIssystem(DEFAULT_GLBCFG_ISSYSTEM)
            .glbcfgCtime(DEFAULT_GLBCFG_CTIME)
            .glbcfgDesc(DEFAULT_GLBCFG_DESC)
            .glbcfgDispidx(DEFAULT_GLBCFG_DISPIDX)
            .glbcfgId(DEFAULT_GLBCFG_ID)
            .glbcfgIsencrypt(DEFAULT_GLBCFG_ISENCRYPT);
        return mxpmsCommGblcfg;
    }

    @Before
    public void initTest() {
        mxpmsCommGblcfg = createEntity(em);
    }

    @Test
    @Transactional
    public void createMxpmsCommGblcfg() throws Exception {
        int databaseSizeBeforeCreate = mxpmsCommGblcfgRepository.findAll().size();

        // Create the MxpmsCommGblcfg
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO = mxpmsCommGblcfgMapper.toDto(mxpmsCommGblcfg);
        restMxpmsCommGblcfgMockMvc.perform(post("/api/mxpms-comm-gblcfgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mxpmsCommGblcfgDTO)))
            .andExpect(status().isCreated());

        // Validate the MxpmsCommGblcfg in the database
        List<MxpmsCommGblcfg> mxpmsCommGblcfgList = mxpmsCommGblcfgRepository.findAll();
        assertThat(mxpmsCommGblcfgList).hasSize(databaseSizeBeforeCreate + 1);
        MxpmsCommGblcfg testMxpmsCommGblcfg = mxpmsCommGblcfgList.get(mxpmsCommGblcfgList.size() - 1);
        assertThat(testMxpmsCommGblcfg.getGlbcfgKey()).isEqualTo(DEFAULT_GLBCFG_KEY);
        assertThat(testMxpmsCommGblcfg.getGlbcfgValue()).isEqualTo(DEFAULT_GLBCFG_VALUE);
        assertThat(testMxpmsCommGblcfg.getGlbcfgName()).isEqualTo(DEFAULT_GLBCFG_NAME);
        assertThat(testMxpmsCommGblcfg.getGlbcfgIssystem()).isEqualTo(DEFAULT_GLBCFG_ISSYSTEM);
        assertThat(testMxpmsCommGblcfg.getGlbcfgCtime()).isEqualTo(DEFAULT_GLBCFG_CTIME);
        assertThat(testMxpmsCommGblcfg.getGlbcfgDesc()).isEqualTo(DEFAULT_GLBCFG_DESC);
        assertThat(testMxpmsCommGblcfg.getGlbcfgDispidx()).isEqualTo(DEFAULT_GLBCFG_DISPIDX);
        assertThat(testMxpmsCommGblcfg.getGlbcfgId()).isEqualTo(DEFAULT_GLBCFG_ID);
        assertThat(testMxpmsCommGblcfg.getGlbcfgIsencrypt()).isEqualTo(DEFAULT_GLBCFG_ISENCRYPT);
    }

    @Test
    @Transactional
    public void createMxpmsCommGblcfgWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mxpmsCommGblcfgRepository.findAll().size();

        // Create the MxpmsCommGblcfg with an existing ID
        mxpmsCommGblcfg.setId(1L);
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO = mxpmsCommGblcfgMapper.toDto(mxpmsCommGblcfg);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMxpmsCommGblcfgMockMvc.perform(post("/api/mxpms-comm-gblcfgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mxpmsCommGblcfgDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<MxpmsCommGblcfg> mxpmsCommGblcfgList = mxpmsCommGblcfgRepository.findAll();
        assertThat(mxpmsCommGblcfgList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMxpmsCommGblcfgs() throws Exception {
        // Initialize the database
        mxpmsCommGblcfgRepository.saveAndFlush(mxpmsCommGblcfg);

        // Get all the mxpmsCommGblcfgList
        restMxpmsCommGblcfgMockMvc.perform(get("/api/mxpms-comm-gblcfgs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mxpmsCommGblcfg.getId().intValue())))
            .andExpect(jsonPath("$.[*].glbcfgKey").value(hasItem(DEFAULT_GLBCFG_KEY.toString())))
            .andExpect(jsonPath("$.[*].glbcfgValue").value(hasItem(DEFAULT_GLBCFG_VALUE.toString())))
            .andExpect(jsonPath("$.[*].glbcfgName").value(hasItem(DEFAULT_GLBCFG_NAME.toString())))
            .andExpect(jsonPath("$.[*].glbcfgIssystem").value(hasItem(DEFAULT_GLBCFG_ISSYSTEM.toString())))
            .andExpect(jsonPath("$.[*].glbcfgCtime").value(hasItem(DEFAULT_GLBCFG_CTIME.toString())))
            .andExpect(jsonPath("$.[*].glbcfgDesc").value(hasItem(DEFAULT_GLBCFG_DESC.toString())))
            .andExpect(jsonPath("$.[*].glbcfgDispidx").value(hasItem(DEFAULT_GLBCFG_DISPIDX.intValue())))
            .andExpect(jsonPath("$.[*].glbcfgId").value(hasItem(DEFAULT_GLBCFG_ID.toString())))
            .andExpect(jsonPath("$.[*].glbcfgIsencrypt").value(hasItem(DEFAULT_GLBCFG_ISENCRYPT.toString())));
    }

    @Test
    @Transactional
    public void getMxpmsCommGblcfg() throws Exception {
        // Initialize the database
        mxpmsCommGblcfgRepository.saveAndFlush(mxpmsCommGblcfg);

        // Get the mxpmsCommGblcfg
        restMxpmsCommGblcfgMockMvc.perform(get("/api/mxpms-comm-gblcfgs/{id}", mxpmsCommGblcfg.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mxpmsCommGblcfg.getId().intValue()))
            .andExpect(jsonPath("$.glbcfgKey").value(DEFAULT_GLBCFG_KEY.toString()))
            .andExpect(jsonPath("$.glbcfgValue").value(DEFAULT_GLBCFG_VALUE.toString()))
            .andExpect(jsonPath("$.glbcfgName").value(DEFAULT_GLBCFG_NAME.toString()))
            .andExpect(jsonPath("$.glbcfgIssystem").value(DEFAULT_GLBCFG_ISSYSTEM.toString()))
            .andExpect(jsonPath("$.glbcfgCtime").value(DEFAULT_GLBCFG_CTIME.toString()))
            .andExpect(jsonPath("$.glbcfgDesc").value(DEFAULT_GLBCFG_DESC.toString()))
            .andExpect(jsonPath("$.glbcfgDispidx").value(DEFAULT_GLBCFG_DISPIDX.intValue()))
            .andExpect(jsonPath("$.glbcfgId").value(DEFAULT_GLBCFG_ID.toString()))
            .andExpect(jsonPath("$.glbcfgIsencrypt").value(DEFAULT_GLBCFG_ISENCRYPT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMxpmsCommGblcfg() throws Exception {
        // Get the mxpmsCommGblcfg
        restMxpmsCommGblcfgMockMvc.perform(get("/api/mxpms-comm-gblcfgs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMxpmsCommGblcfg() throws Exception {
        // Initialize the database
        mxpmsCommGblcfgRepository.saveAndFlush(mxpmsCommGblcfg);
        int databaseSizeBeforeUpdate = mxpmsCommGblcfgRepository.findAll().size();

        // Update the mxpmsCommGblcfg
        MxpmsCommGblcfg updatedMxpmsCommGblcfg = mxpmsCommGblcfgRepository.findOne(mxpmsCommGblcfg.getId());
        updatedMxpmsCommGblcfg
            .glbcfgKey(UPDATED_GLBCFG_KEY)
            .glbcfgValue(UPDATED_GLBCFG_VALUE)
            .glbcfgName(UPDATED_GLBCFG_NAME)
            .glbcfgIssystem(UPDATED_GLBCFG_ISSYSTEM)
            .glbcfgCtime(UPDATED_GLBCFG_CTIME)
            .glbcfgDesc(UPDATED_GLBCFG_DESC)
            .glbcfgDispidx(UPDATED_GLBCFG_DISPIDX)
            .glbcfgId(UPDATED_GLBCFG_ID)
            .glbcfgIsencrypt(UPDATED_GLBCFG_ISENCRYPT);
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO = mxpmsCommGblcfgMapper.toDto(updatedMxpmsCommGblcfg);

        restMxpmsCommGblcfgMockMvc.perform(put("/api/mxpms-comm-gblcfgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mxpmsCommGblcfgDTO)))
            .andExpect(status().isOk());

        // Validate the MxpmsCommGblcfg in the database
        List<MxpmsCommGblcfg> mxpmsCommGblcfgList = mxpmsCommGblcfgRepository.findAll();
        assertThat(mxpmsCommGblcfgList).hasSize(databaseSizeBeforeUpdate);
        MxpmsCommGblcfg testMxpmsCommGblcfg = mxpmsCommGblcfgList.get(mxpmsCommGblcfgList.size() - 1);
        assertThat(testMxpmsCommGblcfg.getGlbcfgKey()).isEqualTo(UPDATED_GLBCFG_KEY);
        assertThat(testMxpmsCommGblcfg.getGlbcfgValue()).isEqualTo(UPDATED_GLBCFG_VALUE);
        assertThat(testMxpmsCommGblcfg.getGlbcfgName()).isEqualTo(UPDATED_GLBCFG_NAME);
        assertThat(testMxpmsCommGblcfg.getGlbcfgIssystem()).isEqualTo(UPDATED_GLBCFG_ISSYSTEM);
        assertThat(testMxpmsCommGblcfg.getGlbcfgCtime()).isEqualTo(UPDATED_GLBCFG_CTIME);
        assertThat(testMxpmsCommGblcfg.getGlbcfgDesc()).isEqualTo(UPDATED_GLBCFG_DESC);
        assertThat(testMxpmsCommGblcfg.getGlbcfgDispidx()).isEqualTo(UPDATED_GLBCFG_DISPIDX);
        assertThat(testMxpmsCommGblcfg.getGlbcfgId()).isEqualTo(UPDATED_GLBCFG_ID);
        assertThat(testMxpmsCommGblcfg.getGlbcfgIsencrypt()).isEqualTo(UPDATED_GLBCFG_ISENCRYPT);
    }

    @Test
    @Transactional
    public void updateNonExistingMxpmsCommGblcfg() throws Exception {
        int databaseSizeBeforeUpdate = mxpmsCommGblcfgRepository.findAll().size();

        // Create the MxpmsCommGblcfg
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO = mxpmsCommGblcfgMapper.toDto(mxpmsCommGblcfg);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMxpmsCommGblcfgMockMvc.perform(put("/api/mxpms-comm-gblcfgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mxpmsCommGblcfgDTO)))
            .andExpect(status().isCreated());

        // Validate the MxpmsCommGblcfg in the database
        List<MxpmsCommGblcfg> mxpmsCommGblcfgList = mxpmsCommGblcfgRepository.findAll();
        assertThat(mxpmsCommGblcfgList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMxpmsCommGblcfg() throws Exception {
        // Initialize the database
        mxpmsCommGblcfgRepository.saveAndFlush(mxpmsCommGblcfg);
        int databaseSizeBeforeDelete = mxpmsCommGblcfgRepository.findAll().size();

        // Get the mxpmsCommGblcfg
        restMxpmsCommGblcfgMockMvc.perform(delete("/api/mxpms-comm-gblcfgs/{id}", mxpmsCommGblcfg.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MxpmsCommGblcfg> mxpmsCommGblcfgList = mxpmsCommGblcfgRepository.findAll();
        assertThat(mxpmsCommGblcfgList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MxpmsCommGblcfg.class);
        MxpmsCommGblcfg mxpmsCommGblcfg1 = new MxpmsCommGblcfg();
        mxpmsCommGblcfg1.setId(1L);
        MxpmsCommGblcfg mxpmsCommGblcfg2 = new MxpmsCommGblcfg();
        mxpmsCommGblcfg2.setId(mxpmsCommGblcfg1.getId());
        assertThat(mxpmsCommGblcfg1).isEqualTo(mxpmsCommGblcfg2);
        mxpmsCommGblcfg2.setId(2L);
        assertThat(mxpmsCommGblcfg1).isNotEqualTo(mxpmsCommGblcfg2);
        mxpmsCommGblcfg1.setId(null);
        assertThat(mxpmsCommGblcfg1).isNotEqualTo(mxpmsCommGblcfg2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MxpmsCommGblcfgDTO.class);
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO1 = new MxpmsCommGblcfgDTO();
        mxpmsCommGblcfgDTO1.setId(1L);
        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO2 = new MxpmsCommGblcfgDTO();
        assertThat(mxpmsCommGblcfgDTO1).isNotEqualTo(mxpmsCommGblcfgDTO2);
        mxpmsCommGblcfgDTO2.setId(mxpmsCommGblcfgDTO1.getId());
        assertThat(mxpmsCommGblcfgDTO1).isEqualTo(mxpmsCommGblcfgDTO2);
        mxpmsCommGblcfgDTO2.setId(2L);
        assertThat(mxpmsCommGblcfgDTO1).isNotEqualTo(mxpmsCommGblcfgDTO2);
        mxpmsCommGblcfgDTO1.setId(null);
        assertThat(mxpmsCommGblcfgDTO1).isNotEqualTo(mxpmsCommGblcfgDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(mxpmsCommGblcfgMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(mxpmsCommGblcfgMapper.fromId(null)).isNull();
    }
}
