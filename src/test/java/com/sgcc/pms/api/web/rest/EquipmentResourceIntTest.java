package com.sgcc.pms.api.web.rest;

import com.sgcc.pms.api.ApiApp;

import com.sgcc.pms.api.domain.Equipment;
import com.sgcc.pms.api.repository.EquipmentRepository;
import com.sgcc.pms.api.service.dto.EquipmentDTO;
import com.sgcc.pms.api.service.mapper.EquipmentMapper;
import com.sgcc.pms.api.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EquipmentResource REST controller.
 *
 * @see EquipmentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApp.class)
public class EquipmentResourceIntTest {

    private static final String DEFAULT_OBJ_ID = "AAAAAAAAAA";
    private static final String UPDATED_OBJ_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PID = "AAAAAAAAAA";
    private static final String UPDATED_PID = "BBBBBBBBBB";

    private static final String DEFAULT_HASCHILDREN = "AAAAAAAAAA";
    private static final String UPDATED_HASCHILDREN = "BBBBBBBBBB";

    private static final String DEFAULT_IMGURL = "AAAAAAAAAA";
    private static final String UPDATED_IMGURL = "BBBBBBBBBB";

    private static final String DEFAULT_TREEID = "AAAAAAAAAA";
    private static final String UPDATED_TREEID = "BBBBBBBBBB";

    private static final String DEFAULT_EXVALUE = "AAAAAAAAAA";
    private static final String UPDATED_EXVALUE = "BBBBBBBBBB";

    private static final String DEFAULT_NODEMODEL = "AAAAAAAAAA";
    private static final String UPDATED_NODEMODEL = "BBBBBBBBBB";

    private static final String DEFAULT_ITEMTYPE = "AAAAAAAAAA";
    private static final String UPDATED_ITEMTYPE = "BBBBBBBBBB";

    private static final String DEFAULT_ORDERID = "AAAAAAAAAA";
    private static final String UPDATED_ORDERID = "BBBBBBBBBB";

    private static final String DEFAULT_NODEWHBZ = "AAAAAAAAAA";
    private static final String UPDATED_NODEWHBZ = "BBBBBBBBBB";

    private static final String DEFAULT_NODEYXZT = "AAAAAAAAAA";
    private static final String UPDATED_NODEYXZT = "BBBBBBBBBB";

    private static final String DEFAULT_ORGID = "AAAAAAAAAA";
    private static final String UPDATED_ORGID = "BBBBBBBBBB";

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEquipmentMockMvc;

    private Equipment equipment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EquipmentResource equipmentResource = new EquipmentResource(equipmentRepository, equipmentMapper);
        this.restEquipmentMockMvc = MockMvcBuilders.standaloneSetup(equipmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipment createEntity(EntityManager em) {
        Equipment equipment = new Equipment()
            .objId(DEFAULT_OBJ_ID)
            .name(DEFAULT_NAME)
            .pid(DEFAULT_PID)
            .haschildren(DEFAULT_HASCHILDREN)
            .imgurl(DEFAULT_IMGURL)
            .treeid(DEFAULT_TREEID)
            .exvalue(DEFAULT_EXVALUE)
            .nodemodel(DEFAULT_NODEMODEL)
            .itemtype(DEFAULT_ITEMTYPE)
            .orderid(DEFAULT_ORDERID)
            .nodewhbz(DEFAULT_NODEWHBZ)
            .nodeyxzt(DEFAULT_NODEYXZT)
            .orgid(DEFAULT_ORGID);
        return equipment;
    }

    @Before
    public void initTest() {
        equipment = createEntity(em);
    }

    @Test
    @Transactional
    public void createEquipment() throws Exception {
        int databaseSizeBeforeCreate = equipmentRepository.findAll().size();

        // Create the Equipment
        EquipmentDTO equipmentDTO = equipmentMapper.toDto(equipment);
        restEquipmentMockMvc.perform(post("/api/equipment")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipmentDTO)))
            .andExpect(status().isCreated());

        // Validate the Equipment in the database
        List<Equipment> equipmentList = equipmentRepository.findAll();
        assertThat(equipmentList).hasSize(databaseSizeBeforeCreate + 1);
        Equipment testEquipment = equipmentList.get(equipmentList.size() - 1);
        assertThat(testEquipment.getObjId()).isEqualTo(DEFAULT_OBJ_ID);
        assertThat(testEquipment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEquipment.getPid()).isEqualTo(DEFAULT_PID);
        assertThat(testEquipment.getHaschildren()).isEqualTo(DEFAULT_HASCHILDREN);
        assertThat(testEquipment.getImgurl()).isEqualTo(DEFAULT_IMGURL);
        assertThat(testEquipment.getTreeid()).isEqualTo(DEFAULT_TREEID);
        assertThat(testEquipment.getExvalue()).isEqualTo(DEFAULT_EXVALUE);
        assertThat(testEquipment.getNodemodel()).isEqualTo(DEFAULT_NODEMODEL);
        assertThat(testEquipment.getItemtype()).isEqualTo(DEFAULT_ITEMTYPE);
        assertThat(testEquipment.getOrderid()).isEqualTo(DEFAULT_ORDERID);
        assertThat(testEquipment.getNodewhbz()).isEqualTo(DEFAULT_NODEWHBZ);
        assertThat(testEquipment.getNodeyxzt()).isEqualTo(DEFAULT_NODEYXZT);
        assertThat(testEquipment.getOrgid()).isEqualTo(DEFAULT_ORGID);
    }

    @Test
    @Transactional
    public void createEquipmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = equipmentRepository.findAll().size();

        // Create the Equipment with an existing ID
        equipment.setId(1L);
        EquipmentDTO equipmentDTO = equipmentMapper.toDto(equipment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEquipmentMockMvc.perform(post("/api/equipment")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Equipment> equipmentList = equipmentRepository.findAll();
        assertThat(equipmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEquipment() throws Exception {
        // Initialize the database
        equipmentRepository.saveAndFlush(equipment);

        // Get all the equipmentList
        restEquipmentMockMvc.perform(get("/api/equipment?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(equipment.getId().intValue())))
            .andExpect(jsonPath("$.[*].objId").value(hasItem(DEFAULT_OBJ_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].pid").value(hasItem(DEFAULT_PID.toString())))
            .andExpect(jsonPath("$.[*].haschildren").value(hasItem(DEFAULT_HASCHILDREN.toString())))
            .andExpect(jsonPath("$.[*].imgurl").value(hasItem(DEFAULT_IMGURL.toString())))
            .andExpect(jsonPath("$.[*].treeid").value(hasItem(DEFAULT_TREEID.toString())))
            .andExpect(jsonPath("$.[*].exvalue").value(hasItem(DEFAULT_EXVALUE.toString())))
            .andExpect(jsonPath("$.[*].nodemodel").value(hasItem(DEFAULT_NODEMODEL.toString())))
            .andExpect(jsonPath("$.[*].itemtype").value(hasItem(DEFAULT_ITEMTYPE.toString())))
            .andExpect(jsonPath("$.[*].orderid").value(hasItem(DEFAULT_ORDERID.toString())))
            .andExpect(jsonPath("$.[*].nodewhbz").value(hasItem(DEFAULT_NODEWHBZ.toString())))
            .andExpect(jsonPath("$.[*].nodeyxzt").value(hasItem(DEFAULT_NODEYXZT.toString())))
            .andExpect(jsonPath("$.[*].orgid").value(hasItem(DEFAULT_ORGID.toString())));
    }

    @Test
    @Transactional
    public void getEquipment() throws Exception {
        // Initialize the database
        equipmentRepository.saveAndFlush(equipment);

        // Get the equipment
        restEquipmentMockMvc.perform(get("/api/equipment/{id}", equipment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(equipment.getId().intValue()))
            .andExpect(jsonPath("$.objId").value(DEFAULT_OBJ_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.pid").value(DEFAULT_PID.toString()))
            .andExpect(jsonPath("$.haschildren").value(DEFAULT_HASCHILDREN.toString()))
            .andExpect(jsonPath("$.imgurl").value(DEFAULT_IMGURL.toString()))
            .andExpect(jsonPath("$.treeid").value(DEFAULT_TREEID.toString()))
            .andExpect(jsonPath("$.exvalue").value(DEFAULT_EXVALUE.toString()))
            .andExpect(jsonPath("$.nodemodel").value(DEFAULT_NODEMODEL.toString()))
            .andExpect(jsonPath("$.itemtype").value(DEFAULT_ITEMTYPE.toString()))
            .andExpect(jsonPath("$.orderid").value(DEFAULT_ORDERID.toString()))
            .andExpect(jsonPath("$.nodewhbz").value(DEFAULT_NODEWHBZ.toString()))
            .andExpect(jsonPath("$.nodeyxzt").value(DEFAULT_NODEYXZT.toString()))
            .andExpect(jsonPath("$.orgid").value(DEFAULT_ORGID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEquipment() throws Exception {
        // Get the equipment
        restEquipmentMockMvc.perform(get("/api/equipment/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEquipment() throws Exception {
        // Initialize the database
        equipmentRepository.saveAndFlush(equipment);
        int databaseSizeBeforeUpdate = equipmentRepository.findAll().size();

        // Update the equipment
        Equipment updatedEquipment = equipmentRepository.findOne(equipment.getId());
        updatedEquipment
            .objId(UPDATED_OBJ_ID)
            .name(UPDATED_NAME)
            .pid(UPDATED_PID)
            .haschildren(UPDATED_HASCHILDREN)
            .imgurl(UPDATED_IMGURL)
            .treeid(UPDATED_TREEID)
            .exvalue(UPDATED_EXVALUE)
            .nodemodel(UPDATED_NODEMODEL)
            .itemtype(UPDATED_ITEMTYPE)
            .orderid(UPDATED_ORDERID)
            .nodewhbz(UPDATED_NODEWHBZ)
            .nodeyxzt(UPDATED_NODEYXZT)
            .orgid(UPDATED_ORGID);
        EquipmentDTO equipmentDTO = equipmentMapper.toDto(updatedEquipment);

        restEquipmentMockMvc.perform(put("/api/equipment")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipmentDTO)))
            .andExpect(status().isOk());

        // Validate the Equipment in the database
        List<Equipment> equipmentList = equipmentRepository.findAll();
        assertThat(equipmentList).hasSize(databaseSizeBeforeUpdate);
        Equipment testEquipment = equipmentList.get(equipmentList.size() - 1);
        assertThat(testEquipment.getObjId()).isEqualTo(UPDATED_OBJ_ID);
        assertThat(testEquipment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEquipment.getPid()).isEqualTo(UPDATED_PID);
        assertThat(testEquipment.getHaschildren()).isEqualTo(UPDATED_HASCHILDREN);
        assertThat(testEquipment.getImgurl()).isEqualTo(UPDATED_IMGURL);
        assertThat(testEquipment.getTreeid()).isEqualTo(UPDATED_TREEID);
        assertThat(testEquipment.getExvalue()).isEqualTo(UPDATED_EXVALUE);
        assertThat(testEquipment.getNodemodel()).isEqualTo(UPDATED_NODEMODEL);
        assertThat(testEquipment.getItemtype()).isEqualTo(UPDATED_ITEMTYPE);
        assertThat(testEquipment.getOrderid()).isEqualTo(UPDATED_ORDERID);
        assertThat(testEquipment.getNodewhbz()).isEqualTo(UPDATED_NODEWHBZ);
        assertThat(testEquipment.getNodeyxzt()).isEqualTo(UPDATED_NODEYXZT);
        assertThat(testEquipment.getOrgid()).isEqualTo(UPDATED_ORGID);
    }

    @Test
    @Transactional
    public void updateNonExistingEquipment() throws Exception {
        int databaseSizeBeforeUpdate = equipmentRepository.findAll().size();

        // Create the Equipment
        EquipmentDTO equipmentDTO = equipmentMapper.toDto(equipment);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEquipmentMockMvc.perform(put("/api/equipment")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(equipmentDTO)))
            .andExpect(status().isCreated());

        // Validate the Equipment in the database
        List<Equipment> equipmentList = equipmentRepository.findAll();
        assertThat(equipmentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEquipment() throws Exception {
        // Initialize the database
        equipmentRepository.saveAndFlush(equipment);
        int databaseSizeBeforeDelete = equipmentRepository.findAll().size();

        // Get the equipment
        restEquipmentMockMvc.perform(delete("/api/equipment/{id}", equipment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Equipment> equipmentList = equipmentRepository.findAll();
        assertThat(equipmentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Equipment.class);
        Equipment equipment1 = new Equipment();
        equipment1.setId(1L);
        Equipment equipment2 = new Equipment();
        equipment2.setId(equipment1.getId());
        assertThat(equipment1).isEqualTo(equipment2);
        equipment2.setId(2L);
        assertThat(equipment1).isNotEqualTo(equipment2);
        equipment1.setId(null);
        assertThat(equipment1).isNotEqualTo(equipment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EquipmentDTO.class);
        EquipmentDTO equipmentDTO1 = new EquipmentDTO();
        equipmentDTO1.setId(1L);
        EquipmentDTO equipmentDTO2 = new EquipmentDTO();
        assertThat(equipmentDTO1).isNotEqualTo(equipmentDTO2);
        equipmentDTO2.setId(equipmentDTO1.getId());
        assertThat(equipmentDTO1).isEqualTo(equipmentDTO2);
        equipmentDTO2.setId(2L);
        assertThat(equipmentDTO1).isNotEqualTo(equipmentDTO2);
        equipmentDTO1.setId(null);
        assertThat(equipmentDTO1).isNotEqualTo(equipmentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(equipmentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(equipmentMapper.fromId(null)).isNull();
    }
}
