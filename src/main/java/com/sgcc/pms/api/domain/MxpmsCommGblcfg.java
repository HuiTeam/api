package com.sgcc.pms.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * The Mxpms_comm_glbcfg entity.
 */
@ApiModel(description = "The Mxpms_comm_glbcfg entity.")
@Entity
@Table(name = "mxpms_comm_gblcfg")
public class MxpmsCommGblcfg implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The glbcfg_key attribute.
     */
    @ApiModelProperty(value = "The glbcfg_key attribute.")
    @Column(name = "glbcfg_key")
    private String glbcfgKey;

    @Column(name = "glbcfg_value")
    private String glbcfgValue;

    @Column(name = "glbcfg_name")
    private String glbcfgName;

    @Column(name = "glbcfg_issystem")
    private String glbcfgIssystem;

    @Column(name = "glbcfg_ctime")
    private Instant glbcfgCtime;

    @Column(name = "glbcfg_desc")
    private String glbcfgDesc;

    @Column(name = "glbcfg_dispidx")
    private Long glbcfgDispidx;

    @Column(name = "glbcfg_id")
    private String glbcfgId;

    @Column(name = "glbcfg_isencrypt")
    private String glbcfgIsencrypt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGlbcfgKey() {
        return glbcfgKey;
    }

    public MxpmsCommGblcfg glbcfgKey(String glbcfgKey) {
        this.glbcfgKey = glbcfgKey;
        return this;
    }

    public void setGlbcfgKey(String glbcfgKey) {
        this.glbcfgKey = glbcfgKey;
    }

    public String getGlbcfgValue() {
        return glbcfgValue;
    }

    public MxpmsCommGblcfg glbcfgValue(String glbcfgValue) {
        this.glbcfgValue = glbcfgValue;
        return this;
    }

    public void setGlbcfgValue(String glbcfgValue) {
        this.glbcfgValue = glbcfgValue;
    }

    public String getGlbcfgName() {
        return glbcfgName;
    }

    public MxpmsCommGblcfg glbcfgName(String glbcfgName) {
        this.glbcfgName = glbcfgName;
        return this;
    }

    public void setGlbcfgName(String glbcfgName) {
        this.glbcfgName = glbcfgName;
    }

    public String getGlbcfgIssystem() {
        return glbcfgIssystem;
    }

    public MxpmsCommGblcfg glbcfgIssystem(String glbcfgIssystem) {
        this.glbcfgIssystem = glbcfgIssystem;
        return this;
    }

    public void setGlbcfgIssystem(String glbcfgIssystem) {
        this.glbcfgIssystem = glbcfgIssystem;
    }

    public Instant getGlbcfgCtime() {
        return glbcfgCtime;
    }

    public MxpmsCommGblcfg glbcfgCtime(Instant glbcfgCtime) {
        this.glbcfgCtime = glbcfgCtime;
        return this;
    }

    public void setGlbcfgCtime(Instant glbcfgCtime) {
        this.glbcfgCtime = glbcfgCtime;
    }

    public String getGlbcfgDesc() {
        return glbcfgDesc;
    }

    public MxpmsCommGblcfg glbcfgDesc(String glbcfgDesc) {
        this.glbcfgDesc = glbcfgDesc;
        return this;
    }

    public void setGlbcfgDesc(String glbcfgDesc) {
        this.glbcfgDesc = glbcfgDesc;
    }

    public Long getGlbcfgDispidx() {
        return glbcfgDispidx;
    }

    public MxpmsCommGblcfg glbcfgDispidx(Long glbcfgDispidx) {
        this.glbcfgDispidx = glbcfgDispidx;
        return this;
    }

    public void setGlbcfgDispidx(Long glbcfgDispidx) {
        this.glbcfgDispidx = glbcfgDispidx;
    }

    public String getGlbcfgId() {
        return glbcfgId;
    }

    public MxpmsCommGblcfg glbcfgId(String glbcfgId) {
        this.glbcfgId = glbcfgId;
        return this;
    }

    public void setGlbcfgId(String glbcfgId) {
        this.glbcfgId = glbcfgId;
    }

    public String getGlbcfgIsencrypt() {
        return glbcfgIsencrypt;
    }

    public MxpmsCommGblcfg glbcfgIsencrypt(String glbcfgIsencrypt) {
        this.glbcfgIsencrypt = glbcfgIsencrypt;
        return this;
    }

    public void setGlbcfgIsencrypt(String glbcfgIsencrypt) {
        this.glbcfgIsencrypt = glbcfgIsencrypt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MxpmsCommGblcfg mxpmsCommGblcfg = (MxpmsCommGblcfg) o;
        if (mxpmsCommGblcfg.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mxpmsCommGblcfg.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MxpmsCommGblcfg{" +
            "id=" + getId() +
            ", glbcfgKey='" + getGlbcfgKey() + "'" +
            ", glbcfgValue='" + getGlbcfgValue() + "'" +
            ", glbcfgName='" + getGlbcfgName() + "'" +
            ", glbcfgIssystem='" + getGlbcfgIssystem() + "'" +
            ", glbcfgCtime='" + getGlbcfgCtime() + "'" +
            ", glbcfgDesc='" + getGlbcfgDesc() + "'" +
            ", glbcfgDispidx='" + getGlbcfgDispidx() + "'" +
            ", glbcfgId='" + getGlbcfgId() + "'" +
            ", glbcfgIsencrypt='" + getGlbcfgIsencrypt() + "'" +
            "}";
    }
}
