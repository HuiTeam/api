package com.sgcc.pms.api.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Monitor.
 */
@Entity
@Table(name = "monitor")
public class Monitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Monitor name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Monitor monitor = (Monitor) o;
        if (monitor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), monitor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Monitor{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
