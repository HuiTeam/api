package com.sgcc.pms.api.domain;

/**
 * Created by qk on 17/10/31.
 */

public class Greeting {

    private String content;

    public Greeting() {
    }

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
