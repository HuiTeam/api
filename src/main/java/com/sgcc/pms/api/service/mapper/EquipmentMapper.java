package com.sgcc.pms.api.service.mapper;

import com.sgcc.pms.api.domain.*;
import com.sgcc.pms.api.service.dto.EquipmentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Equipment and its DTO EquipmentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EquipmentMapper extends EntityMapper <EquipmentDTO, Equipment> {
    
    
    default Equipment fromId(Long id) {
        if (id == null) {
            return null;
        }
        Equipment equipment = new Equipment();
        equipment.setId(id);
        return equipment;
    }
}
