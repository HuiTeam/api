package com.sgcc.pms.api.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MxpmsCommGblcfg entity.
 */
public class MxpmsCommGblcfgDTO implements Serializable {

    private Long id;

    private String glbcfgKey;

    private String glbcfgValue;

    private String glbcfgName;

    private String glbcfgIssystem;

    private Instant glbcfgCtime;

    private String glbcfgDesc;

    private Long glbcfgDispidx;

    private String glbcfgId;

    private String glbcfgIsencrypt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGlbcfgKey() {
        return glbcfgKey;
    }

    public void setGlbcfgKey(String glbcfgKey) {
        this.glbcfgKey = glbcfgKey;
    }

    public String getGlbcfgValue() {
        return glbcfgValue;
    }

    public void setGlbcfgValue(String glbcfgValue) {
        this.glbcfgValue = glbcfgValue;
    }

    public String getGlbcfgName() {
        return glbcfgName;
    }

    public void setGlbcfgName(String glbcfgName) {
        this.glbcfgName = glbcfgName;
    }

    public String getGlbcfgIssystem() {
        return glbcfgIssystem;
    }

    public void setGlbcfgIssystem(String glbcfgIssystem) {
        this.glbcfgIssystem = glbcfgIssystem;
    }

    public Instant getGlbcfgCtime() {
        return glbcfgCtime;
    }

    public void setGlbcfgCtime(Instant glbcfgCtime) {
        this.glbcfgCtime = glbcfgCtime;
    }

    public String getGlbcfgDesc() {
        return glbcfgDesc;
    }

    public void setGlbcfgDesc(String glbcfgDesc) {
        this.glbcfgDesc = glbcfgDesc;
    }

    public Long getGlbcfgDispidx() {
        return glbcfgDispidx;
    }

    public void setGlbcfgDispidx(Long glbcfgDispidx) {
        this.glbcfgDispidx = glbcfgDispidx;
    }

    public String getGlbcfgId() {
        return glbcfgId;
    }

    public void setGlbcfgId(String glbcfgId) {
        this.glbcfgId = glbcfgId;
    }

    public String getGlbcfgIsencrypt() {
        return glbcfgIsencrypt;
    }

    public void setGlbcfgIsencrypt(String glbcfgIsencrypt) {
        this.glbcfgIsencrypt = glbcfgIsencrypt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MxpmsCommGblcfgDTO mxpmsCommGblcfgDTO = (MxpmsCommGblcfgDTO) o;
        if(mxpmsCommGblcfgDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mxpmsCommGblcfgDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MxpmsCommGblcfgDTO{" +
            "id=" + getId() +
            ", glbcfgKey='" + getGlbcfgKey() + "'" +
            ", glbcfgValue='" + getGlbcfgValue() + "'" +
            ", glbcfgName='" + getGlbcfgName() + "'" +
            ", glbcfgIssystem='" + getGlbcfgIssystem() + "'" +
            ", glbcfgCtime='" + getGlbcfgCtime() + "'" +
            ", glbcfgDesc='" + getGlbcfgDesc() + "'" +
            ", glbcfgDispidx='" + getGlbcfgDispidx() + "'" +
            ", glbcfgId='" + getGlbcfgId() + "'" +
            ", glbcfgIsencrypt='" + getGlbcfgIsencrypt() + "'" +
            "}";
    }
}
