package com.sgcc.pms.api.service.mapper;

import com.sgcc.pms.api.domain.*;
import com.sgcc.pms.api.service.dto.MxpmsCommGblcfgDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MxpmsCommGblcfg and its DTO MxpmsCommGblcfgDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MxpmsCommGblcfgMapper extends EntityMapper <MxpmsCommGblcfgDTO, MxpmsCommGblcfg> {
    
    
    default MxpmsCommGblcfg fromId(Long id) {
        if (id == null) {
            return null;
        }
        MxpmsCommGblcfg mxpmsCommGblcfg = new MxpmsCommGblcfg();
        mxpmsCommGblcfg.setId(id);
        return mxpmsCommGblcfg;
    }
}
