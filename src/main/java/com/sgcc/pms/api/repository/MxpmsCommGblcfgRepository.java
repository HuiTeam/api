package com.sgcc.pms.api.repository;

import com.sgcc.pms.api.domain.MxpmsCommGblcfg;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MxpmsCommGblcfg entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MxpmsCommGblcfgRepository extends JpaRepository<MxpmsCommGblcfg,Long> {
    
}
