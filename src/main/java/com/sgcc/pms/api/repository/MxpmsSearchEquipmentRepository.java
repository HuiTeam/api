package com.sgcc.pms.api.repository;

import com.sgcc.pms.api.domain.MxpmsSearchEquipment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MxpmsSearchEquipment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MxpmsSearchEquipmentRepository extends JpaRepository<MxpmsSearchEquipment,Long> {
    
}
