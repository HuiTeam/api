/**
 * Data Access Objects used by WebSocket services.
 */
package com.sgcc.pms.api.web.websocket.dto;
