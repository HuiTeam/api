import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ApiMxpmsSearchEquipmentMySuffixModule } from './mxpms-search-equipment/mxpms-search-equipment-my-suffix.module';
import { ApiEquipmentMySuffixModule } from './equipment/equipment-my-suffix.module';
import { ApiMonitorModule } from './monitor/monitor.module';
import { ApiMxpmsCommGblcfgMySuffixModule } from './mxpms-comm-gblcfg/mxpms-comm-gblcfg-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ApiMxpmsSearchEquipmentMySuffixModule,
        ApiEquipmentMySuffixModule,
        ApiMonitorModule,
        ApiMxpmsCommGblcfgMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApiEntityModule {}
