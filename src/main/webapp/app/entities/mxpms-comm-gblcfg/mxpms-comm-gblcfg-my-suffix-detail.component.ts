import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { MxpmsCommGblcfgMySuffix } from './mxpms-comm-gblcfg-my-suffix.model';
import { MxpmsCommGblcfgMySuffixService } from './mxpms-comm-gblcfg-my-suffix.service';

@Component({
    selector: 'jhi-mxpms-comm-gblcfg-my-suffix-detail',
    templateUrl: './mxpms-comm-gblcfg-my-suffix-detail.component.html'
})
export class MxpmsCommGblcfgMySuffixDetailComponent implements OnInit, OnDestroy {

    mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private mxpmsCommGblcfgService: MxpmsCommGblcfgMySuffixService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMxpmsCommGblcfgs();
    }

    load(id) {
        this.mxpmsCommGblcfgService.find(id).subscribe((mxpmsCommGblcfg) => {
            this.mxpmsCommGblcfg = mxpmsCommGblcfg;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMxpmsCommGblcfgs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'mxpmsCommGblcfgListModification',
            (response) => this.load(this.mxpmsCommGblcfg.id)
        );
    }
}
