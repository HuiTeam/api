import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MxpmsCommGblcfgMySuffix } from './mxpms-comm-gblcfg-my-suffix.model';
import { MxpmsCommGblcfgMySuffixPopupService } from './mxpms-comm-gblcfg-my-suffix-popup.service';
import { MxpmsCommGblcfgMySuffixService } from './mxpms-comm-gblcfg-my-suffix.service';

@Component({
    selector: 'jhi-mxpms-comm-gblcfg-my-suffix-delete-dialog',
    templateUrl: './mxpms-comm-gblcfg-my-suffix-delete-dialog.component.html'
})
export class MxpmsCommGblcfgMySuffixDeleteDialogComponent {

    mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix;

    constructor(
        private mxpmsCommGblcfgService: MxpmsCommGblcfgMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.mxpmsCommGblcfgService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'mxpmsCommGblcfgListModification',
                content: 'Deleted an mxpmsCommGblcfg'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-mxpms-comm-gblcfg-my-suffix-delete-popup',
    template: ''
})
export class MxpmsCommGblcfgMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mxpmsCommGblcfgPopupService: MxpmsCommGblcfgMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.mxpmsCommGblcfgPopupService
                .open(MxpmsCommGblcfgMySuffixDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
