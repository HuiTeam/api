import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { MxpmsCommGblcfgMySuffix } from './mxpms-comm-gblcfg-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MxpmsCommGblcfgMySuffixService {

    private resourceUrl = 'api/mxpms-comm-gblcfgs';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix): Observable<MxpmsCommGblcfgMySuffix> {
        const copy = this.convert(mxpmsCommGblcfg);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix): Observable<MxpmsCommGblcfgMySuffix> {
        const copy = this.convert(mxpmsCommGblcfg);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<MxpmsCommGblcfgMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.glbcfgCtime = this.dateUtils
            .convertDateTimeFromServer(entity.glbcfgCtime);
    }

    private convert(mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix): MxpmsCommGblcfgMySuffix {
        const copy: MxpmsCommGblcfgMySuffix = Object.assign({}, mxpmsCommGblcfg);

        copy.glbcfgCtime = this.dateUtils.toDate(mxpmsCommGblcfg.glbcfgCtime);
        return copy;
    }
}
