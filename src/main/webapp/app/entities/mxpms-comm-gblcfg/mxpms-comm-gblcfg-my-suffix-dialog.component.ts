import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MxpmsCommGblcfgMySuffix } from './mxpms-comm-gblcfg-my-suffix.model';
import { MxpmsCommGblcfgMySuffixPopupService } from './mxpms-comm-gblcfg-my-suffix-popup.service';
import { MxpmsCommGblcfgMySuffixService } from './mxpms-comm-gblcfg-my-suffix.service';

@Component({
    selector: 'jhi-mxpms-comm-gblcfg-my-suffix-dialog',
    templateUrl: './mxpms-comm-gblcfg-my-suffix-dialog.component.html'
})
export class MxpmsCommGblcfgMySuffixDialogComponent implements OnInit {

    mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private mxpmsCommGblcfgService: MxpmsCommGblcfgMySuffixService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.mxpmsCommGblcfg.id !== undefined) {
            this.subscribeToSaveResponse(
                this.mxpmsCommGblcfgService.update(this.mxpmsCommGblcfg));
        } else {
            this.subscribeToSaveResponse(
                this.mxpmsCommGblcfgService.create(this.mxpmsCommGblcfg));
        }
    }

    private subscribeToSaveResponse(result: Observable<MxpmsCommGblcfgMySuffix>) {
        result.subscribe((res: MxpmsCommGblcfgMySuffix) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: MxpmsCommGblcfgMySuffix) {
        this.eventManager.broadcast({ name: 'mxpmsCommGblcfgListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-mxpms-comm-gblcfg-my-suffix-popup',
    template: ''
})
export class MxpmsCommGblcfgMySuffixPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private mxpmsCommGblcfgPopupService: MxpmsCommGblcfgMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.mxpmsCommGblcfgPopupService
                    .open(MxpmsCommGblcfgMySuffixDialogComponent as Component, params['id']);
            } else {
                this.mxpmsCommGblcfgPopupService
                    .open(MxpmsCommGblcfgMySuffixDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
