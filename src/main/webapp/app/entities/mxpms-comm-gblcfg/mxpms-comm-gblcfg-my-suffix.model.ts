import { BaseEntity } from './../../shared';

export class MxpmsCommGblcfgMySuffix implements BaseEntity {
    constructor(
        public id?: number,
        public glbcfgKey?: string,
        public glbcfgValue?: string,
        public glbcfgName?: string,
        public glbcfgIssystem?: string,
        public glbcfgCtime?: any,
        public glbcfgDesc?: string,
        public glbcfgDispidx?: number,
        public glbcfgId?: string,
        public glbcfgIsencrypt?: string,
    ) {
    }
}
