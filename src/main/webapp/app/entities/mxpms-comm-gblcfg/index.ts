export * from './mxpms-comm-gblcfg-my-suffix.model';
export * from './mxpms-comm-gblcfg-my-suffix-popup.service';
export * from './mxpms-comm-gblcfg-my-suffix.service';
export * from './mxpms-comm-gblcfg-my-suffix-dialog.component';
export * from './mxpms-comm-gblcfg-my-suffix-delete-dialog.component';
export * from './mxpms-comm-gblcfg-my-suffix-detail.component';
export * from './mxpms-comm-gblcfg-my-suffix.component';
export * from './mxpms-comm-gblcfg-my-suffix.route';
