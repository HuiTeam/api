import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ApiSharedModule } from '../../shared';
import {
    MxpmsCommGblcfgMySuffixService,
    MxpmsCommGblcfgMySuffixPopupService,
    MxpmsCommGblcfgMySuffixComponent,
    MxpmsCommGblcfgMySuffixDetailComponent,
    MxpmsCommGblcfgMySuffixDialogComponent,
    MxpmsCommGblcfgMySuffixPopupComponent,
    MxpmsCommGblcfgMySuffixDeletePopupComponent,
    MxpmsCommGblcfgMySuffixDeleteDialogComponent,
    mxpmsCommGblcfgRoute,
    mxpmsCommGblcfgPopupRoute,
} from './';

const ENTITY_STATES = [
    ...mxpmsCommGblcfgRoute,
    ...mxpmsCommGblcfgPopupRoute,
];

@NgModule({
    imports: [
        ApiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MxpmsCommGblcfgMySuffixComponent,
        MxpmsCommGblcfgMySuffixDetailComponent,
        MxpmsCommGblcfgMySuffixDialogComponent,
        MxpmsCommGblcfgMySuffixDeleteDialogComponent,
        MxpmsCommGblcfgMySuffixPopupComponent,
        MxpmsCommGblcfgMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        MxpmsCommGblcfgMySuffixComponent,
        MxpmsCommGblcfgMySuffixDialogComponent,
        MxpmsCommGblcfgMySuffixPopupComponent,
        MxpmsCommGblcfgMySuffixDeleteDialogComponent,
        MxpmsCommGblcfgMySuffixDeletePopupComponent,
    ],
    providers: [
        MxpmsCommGblcfgMySuffixService,
        MxpmsCommGblcfgMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApiMxpmsCommGblcfgMySuffixModule {}
