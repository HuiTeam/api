import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { MxpmsCommGblcfgMySuffix } from './mxpms-comm-gblcfg-my-suffix.model';
import { MxpmsCommGblcfgMySuffixService } from './mxpms-comm-gblcfg-my-suffix.service';

@Injectable()
export class MxpmsCommGblcfgMySuffixPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private mxpmsCommGblcfgService: MxpmsCommGblcfgMySuffixService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.mxpmsCommGblcfgService.find(id).subscribe((mxpmsCommGblcfg) => {
                    mxpmsCommGblcfg.glbcfgCtime = this.datePipe
                        .transform(mxpmsCommGblcfg.glbcfgCtime, 'yyyy-MM-ddThh:mm');
                    this.ngbModalRef = this.mxpmsCommGblcfgModalRef(component, mxpmsCommGblcfg);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.mxpmsCommGblcfgModalRef(component, new MxpmsCommGblcfgMySuffix());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    mxpmsCommGblcfgModalRef(component: Component, mxpmsCommGblcfg: MxpmsCommGblcfgMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.mxpmsCommGblcfg = mxpmsCommGblcfg;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
