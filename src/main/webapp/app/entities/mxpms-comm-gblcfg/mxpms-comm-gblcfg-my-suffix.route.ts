import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MxpmsCommGblcfgMySuffixComponent } from './mxpms-comm-gblcfg-my-suffix.component';
import { MxpmsCommGblcfgMySuffixDetailComponent } from './mxpms-comm-gblcfg-my-suffix-detail.component';
import { MxpmsCommGblcfgMySuffixPopupComponent } from './mxpms-comm-gblcfg-my-suffix-dialog.component';
import { MxpmsCommGblcfgMySuffixDeletePopupComponent } from './mxpms-comm-gblcfg-my-suffix-delete-dialog.component';

export const mxpmsCommGblcfgRoute: Routes = [
    {
        path: 'mxpms-comm-gblcfg-my-suffix',
        component: MxpmsCommGblcfgMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.mxpmsCommGblcfg.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'mxpms-comm-gblcfg-my-suffix/:id',
        component: MxpmsCommGblcfgMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.mxpmsCommGblcfg.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const mxpmsCommGblcfgPopupRoute: Routes = [
    {
        path: 'mxpms-comm-gblcfg-my-suffix-new',
        component: MxpmsCommGblcfgMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.mxpmsCommGblcfg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'mxpms-comm-gblcfg-my-suffix/:id/edit',
        component: MxpmsCommGblcfgMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.mxpmsCommGblcfg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'mxpms-comm-gblcfg-my-suffix/:id/delete',
        component: MxpmsCommGblcfgMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.mxpmsCommGblcfg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
