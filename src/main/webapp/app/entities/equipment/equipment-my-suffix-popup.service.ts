import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EquipmentMySuffix } from './equipment-my-suffix.model';
import { EquipmentMySuffixService } from './equipment-my-suffix.service';

@Injectable()
export class EquipmentMySuffixPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private equipmentService: EquipmentMySuffixService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.equipmentService.find(id).subscribe((equipment) => {
                this.equipmentModalRef(component, equipment);
            });
        } else {
            return this.equipmentModalRef(component, new EquipmentMySuffix());
        }
    }

    equipmentModalRef(component: Component, equipment: EquipmentMySuffix): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.equipment = equipment;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
