import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { EquipmentMySuffix } from './equipment-my-suffix.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EquipmentMySuffixService {

    private resourceUrl = 'api/equipment';

    constructor(private http: Http) { }

    create(equipment: EquipmentMySuffix): Observable<EquipmentMySuffix> {
        const copy = this.convert(equipment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(equipment: EquipmentMySuffix): Observable<EquipmentMySuffix> {
        const copy = this.convert(equipment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<EquipmentMySuffix> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(equipment: EquipmentMySuffix): EquipmentMySuffix {
        const copy: EquipmentMySuffix = Object.assign({}, equipment);
        return copy;
    }
}
