import { BaseEntity } from './../../shared';

export class Monitor implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
