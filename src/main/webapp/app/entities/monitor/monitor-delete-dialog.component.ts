import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Monitor } from './monitor.model';
import { MonitorPopupService } from './monitor-popup.service';
import { MonitorService } from './monitor.service';

@Component({
    selector: 'jhi-monitor-delete-dialog',
    templateUrl: './monitor-delete-dialog.component.html'
})
export class MonitorDeleteDialogComponent {

    monitor: Monitor;

    constructor(
        private monitorService: MonitorService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.monitorService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'monitorListModification',
                content: 'Deleted an monitor'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-monitor-delete-popup',
    template: ''
})
export class MonitorDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private monitorPopupService: MonitorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.monitorPopupService
                .open(MonitorDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
