import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Monitor } from './monitor.model';
import { MonitorService } from './monitor.service';

@Component({
    selector: 'jhi-monitor-detail',
    templateUrl: './monitor-detail.component.html'
})
export class MonitorDetailComponent implements OnInit, OnDestroy {

    monitor: Monitor;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private monitorService: MonitorService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMonitors();
    }

    load(id) {
        this.monitorService.find(id).subscribe((monitor) => {
            this.monitor = monitor;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMonitors() {
        this.eventSubscriber = this.eventManager.subscribe(
            'monitorListModification',
            (response) => this.load(this.monitor.id)
        );
    }
}
