import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MonitorComponent } from './monitor.component';
import { MonitorDetailComponent } from './monitor-detail.component';
import { MonitorPopupComponent } from './monitor-dialog.component';
import { MonitorDeletePopupComponent } from './monitor-delete-dialog.component';

@Injectable()
export class MonitorResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const monitorRoute: Routes = [
    {
        path: 'monitor',
        component: MonitorComponent,
        resolve: {
            'pagingParams': MonitorResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.monitor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'monitor/:id',
        component: MonitorDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.monitor.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const monitorPopupRoute: Routes = [
    {
        path: 'monitor-new',
        component: MonitorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.monitor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'monitor/:id/edit',
        component: MonitorPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.monitor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'monitor/:id/delete',
        component: MonitorDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'apiApp.monitor.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
