import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Monitor } from './monitor.model';
import { MonitorPopupService } from './monitor-popup.service';
import { MonitorService } from './monitor.service';

@Component({
    selector: 'jhi-monitor-dialog',
    templateUrl: './monitor-dialog.component.html'
})
export class MonitorDialogComponent implements OnInit {

    monitor: Monitor;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private monitorService: MonitorService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.monitor.id !== undefined) {
            this.subscribeToSaveResponse(
                this.monitorService.update(this.monitor));
        } else {
            this.subscribeToSaveResponse(
                this.monitorService.create(this.monitor));
        }
    }

    private subscribeToSaveResponse(result: Observable<Monitor>) {
        result.subscribe((res: Monitor) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Monitor) {
        this.eventManager.broadcast({ name: 'monitorListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-monitor-popup',
    template: ''
})
export class MonitorPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private monitorPopupService: MonitorPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.monitorPopupService
                    .open(MonitorDialogComponent as Component, params['id']);
            } else {
                this.monitorPopupService
                    .open(MonitorDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
