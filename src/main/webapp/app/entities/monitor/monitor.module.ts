import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ApiSharedModule } from '../../shared';
import {
    MonitorService,
    MonitorPopupService,
    MonitorComponent,
    MonitorDetailComponent,
    MonitorDialogComponent,
    MonitorPopupComponent,
    MonitorDeletePopupComponent,
    MonitorDeleteDialogComponent,
    monitorRoute,
    monitorPopupRoute,
    MonitorResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...monitorRoute,
    ...monitorPopupRoute,
];

@NgModule({
    imports: [
        ApiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MonitorComponent,
        MonitorDetailComponent,
        MonitorDialogComponent,
        MonitorDeleteDialogComponent,
        MonitorPopupComponent,
        MonitorDeletePopupComponent,
    ],
    entryComponents: [
        MonitorComponent,
        MonitorDialogComponent,
        MonitorPopupComponent,
        MonitorDeleteDialogComponent,
        MonitorDeletePopupComponent,
    ],
    providers: [
        MonitorService,
        MonitorPopupService,
        MonitorResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApiMonitorModule {}
