## 扩展功能

#### 1228

| A   |      B      |  C |
|----------|:-------------:|------:|
| 1 | 下拉框 |  |
| 2 | 联动效果 |  |


*   java -jar -Dserver.port=8085 api-0.0.1-SNAPSHOT.war &

| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| 1 | token权限的生成 |  |
| 2 | 用户定制菜单(仅数据库)   |   12 |
| 3 | 按IP访问,访问次数 |    已改为不做限制可以访问 |
| 4 | 完全匹配字符 |    $1 |
| 5 | 部署时日志输出路径 |    $1 |
| 6 | 指定首页跳转 |    $1 |
| 6 | 将charts目录移动到admin目录下 |    $1 |


#### 1205

1、打成war包,添加启动命令

2、可修改端口 OK

3、地址从数据库中读

4、修改首页及logo OK

#### 1128

1、ztree websocket-chart改成 js submodule (TODO)

#### 1125

1、durid

2、jenkins2 pipline 部署脚本

3、oracle prod生产环境打包

4、admin/webjars/index.html 简单聊天室

5、JWT动态获取(TODO)
